import jokes from './jokes.js'


Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        jokes
    },
    strict: false
})