import Api from '../api'
import storage from '../utils/storage.js'

let jokesFromStorage = storage.loadFavouriteJokes()

export default {

    namespaced: true,

    state: {

        jokes: [],
        favourites: jokesFromStorage.favourites,
        cache: jokesFromStorage.cache,
        busy: false

    },

    getters: {
        markedJokes(state) {
            return state.jokes.map(({id, joke})=>{
                return {id, joke, favourite: state.favourites[id] }
            })
        },
        favouriteJokes(state) {
            let jokes = []
            for (var id in state.favourites) {
                if (state.favourites[id]) {
                    jokes.push({
                        id: id,
                        joke: state.cache[id],
                        favourite: true
                    })
                }
            }
            storage.saveFavouriteJokes(jokes)
            return jokes
        }
    },


    mutations: {
        setJokes(state, data) {
            state.jokes = data.value
        },
        setBusy(state, value) {
            state.busy = value
        },
        switchFavourite(state, item) {
            if (state.favourites[item.id]) {
                Vue.set(state.favourites, item.id, false)
            } else {
                Vue.set(state.favourites, item.id, true)
                Vue.set(state.cache, item.id, item.joke)
            }
        },
        addFavourite(state, item) {
            Vue.set(state.favourites, item.id, true)
            Vue.set(state.cache, item.id, item.joke)
        },
        deleteFavourite(state, item) {
            Vue.set(state.favourites, item.id, false)
        }
    },

    actions: {
        loadJokes(state, count) {
            state.commit("setBusy", true)
            Api.getRandom(count)
                .then((response)=>{
                    state.commit("setJokes", response.data)
                })
                .catch()
                .finally(()=>{
                    state.commit("setBusy", false)
                })
        },

        addRandomJoke(state) {
            Api.getRandom(1)
                .then((response)=>{
                    state.commit("addFavourite", response.data.value[0])
                })
                .catch()
                .finally()
        }
    }
}