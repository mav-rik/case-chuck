class Api {
    constructor () {
        this.axios = axios.create({
                baseURL: 'http://api.icndb.com/jokes/'
        })
    }

    getRandom(count) {
        return this.axios.get('random/' + count);
    }
}


export default new Api();