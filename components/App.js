import JokeList from "./JokeList.js";

export default Vue.component('App', {
    template: `
    <v-app>
        <v-toolbar>
            <v-toolbar-title>Chuck-Case</v-toolbar-title>
            <v-spacer></v-spacer>
            <v-btn  
                dark
                :color="timerIsActive ? 'red' : 'primary'"
                @click="timerIsActive = !timerIsActive" 
                :depressed="timerIsActive" >
                Fetch random Joke each {{fetchDelay}} seconds
            </v-btn>
            <v-btn  
                color="primary"
                :loading="busy" 
                @click="onLoadJokes(fetchJokesCount)">
                Fetch {{fetchJokesCount}} Jokes
            </v-btn>
        </v-toolbar>
        <v-content>
            <v-layout>
                <v-flex xs6 class="pa-2">
                    <joke-list 
                        class="elevation-1"
                        title="Random Jokes"
                        actionFavourite
                        @onFavourite="onFavourite"
                        :items="markedJokes">
                    </joke-list>
                </v-flex>
                <v-flex xs6 class="pa-2">
                    <joke-list 
                        class="elevation-1"
                        title="Favourite Jokes"
                        actionDelete
                        @onDelete="onDelete"
                        :items="favouriteJokes">
                    </joke-list>
                </v-flex>
            </v-layout>
        </v-content>
        
        <v-dialog v-model="dialogShow" width="500px">
            <v-card>
                <v-toolbar dense dark class="elevation-0" color="primary">
                    <v-toolbar-title>Can not perform an action</v-toolbar-title>
                </v-toolbar>
                <v-card-text>
                    The maximum number of favourite jokes reached. You can not add any moe jokes.                
                </v-card-text>
                <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn flat color="primary" @click="dialogShow=false">Ok</v-btn>                
                </v-card-actions>
            </v-card>
        </v-dialog>
    </v-app>  
    `,
    computed: {
        ...Vuex.mapState('jokes',['busy', 'jokes']),
        ...Vuex.mapGetters('jokes',['markedJokes', 'favouriteJokes'])
    },
    data: ()=>{
        return {
            fetchJokesCount: 10,
            favouriteJokesCount: 10,
            fetchDelay: 5,
            dialogShow: false,
            timerIsActive: false,
            intervalId: null
        }
    },
    methods: {
        ...Vuex.mapActions('jokes',['loadJokes', 'addRandomJoke']),
        ...Vuex.mapMutations('jokes',['switchFavourite', 'deleteFavourite']),
        onLoadJokes(count) {
            this.loadJokes(count)
        },
        onFavourite(item) {
            if (item.favourite || (this.favouriteJokes.length < this.favouriteJokesCount)) {
                this.switchFavourite(item)
            } else {
                this.dialogShow = true
            }
        },
        onDelete(item) {
            this.deleteFavourite(item);
        }
    },
    watch: {
        timerIsActive(value) {
            if (value) {
                this.intervalId = setInterval(()=>{
                    if (this.favouriteJokes.length < this.favouriteJokesCount) {
                        this.addRandomJoke()
                    } else {
                        this.timerIsActive = false;
                    }
                }, this.fetchDelay*1000)
            } else {
                clearInterval(this.intervalId)
            }
        }
    },
    components: {
        JokeList
    }
})