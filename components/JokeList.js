export default Vue.component('JokeList', {
    template: `
        <v-list>
            <v-subheader>
              {{ title }}
            </v-subheader>
            <v-list-tile v-for="item in items">
                <v-list-tile-content>
                    <v-list-tile-title v-html="item.joke"></v-list-tile-title>
                </v-list-tile-content>
                
                <v-list-tile-action>
                  <v-btn icon ripple>
                    <v-icon v-if="actionFavourite" 
                        :color="item.favourite ? 'yellow darken-1' : 'grey lighten-1'" 
                        @click="$emit('onFavourite',item)">
                        star
                    </v-icon>
                    <v-icon v-if="actionDelete" 
                        color="grey lighten-1" 
                        @click="$emit('onDelete',item)">
                        delete
                    </v-icon>
                  </v-btn>
                </v-list-tile-action>
            </v-list-tile>
        </v-list>
    `,

    props: {
        title: String,
        items: Array,
        actionFavourite: {
            type: Boolean,
            default: false
        },
        actionDelete: {
            type: Boolean,
            default: false
        }
    }
})