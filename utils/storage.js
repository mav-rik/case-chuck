export default {

    saveFavouriteJokes(favouriteJokes) {
        console.log('saveFavouriteJokes')
        localStorage.setItem('favouriteJokes', JSON.stringify(favouriteJokes))
    },

    loadFavouriteJokes() {
        console.log('loadFavouriteJokes')
        let cache = {}
        let favourites = {}
        let jokes = localStorage.getItem('favouriteJokes')
        if (jokes) {
            jokes = JSON.parse(jokes)
            jokes.forEach((joke) => {
                cache[joke.id] = joke.joke
                favourites[joke.id] = true
            })
        }
        console.log({cache, favourites});
        return {cache, favourites}
    }

}